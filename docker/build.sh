#!/usr/bin/env bash
base_repo_url=registry.gitlab.com/dropchop/clippitamine-k8s

build_push() {
  dir=$1
  name=$2
  parent=$3
  ver=$(<${dir}/VERSION)
  docker build ${dir} -t ${base_repo_url}/${parent}/${name}:${ver} -t ${base_repo_url}/${parent}/${name}:latest
  docker push ${base_repo_url}/${parent}/${name}:${ver}
  docker push ${base_repo_url}/${parent}/${name}:latest
}

build_push landing-page landing-page frontend
build_push prometheus-vmware-exporter prometheus-vmware-exporter infrastructure
build_push elastic-utils elastic-utils database
build_push pgadmin4 pgadmin4 database
build_push stolon-postgresql stolon-postgresql database
build_push rabbitmq rabbitmq database
build_push gluster gluster infrastructure
build_push kube-utils kube-utils infrastructure


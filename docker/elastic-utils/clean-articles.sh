#!/usr/bin/env bash

url="${1}"
datefield="${2}"
start="${3}"
end="${4}"

indexname=$(date +article-%Y-%m-01 -d "7 days ago")

if [[ -z "${url}" ]]; then
  url="http://localhost:9200/${indexname}"
fi

if [[ -z "${datefield}" ]]; then
  datefield="created"
fi

if [[ -z "${start}" ]]; then
  start=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "7 days ago")
fi

if [[ -z "${end}" ]]; then
  end=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "6 days ago")
fi

stale_limit=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "7 days ago")

gen_post_data() {
cat <<EOF
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "${3}": {
              "from": "${1}",
              "to": "${2}",
              "include_lower": true,
              "include_upper": false
            }
          }
        },
        {
          "terms": {
            "state.name": [
              "editing"
            ]
          }
        },
        {
          "terms": {
            "media.tags.uuid": [
              "799dea91-d6a6-380a-8f07-f601cc1665a1",
              "1b97b3a7-a57e-323f-8a38-612a5b7d4403",
              "60a485fb-c30d-3a5d-99c1-ba08b78f4fbc",
              "06d1d4ce-90db-3e1d-af6c-a600eb25aaeb",
              "ba3fc091-5a42-3146-bd2e-6b0b87214bac",
              "9df12353-4cde-3b09-af28-3663ecd2e6bd",
              "ba1837be-a948-37c2-89ff-c84442477461",
              "6ac2e9e0-9824-3057-86ff-da5471f8489b",
              "0e083fd8-7f23-374b-baaf-99b3297a5fca",
              "7b899cd8-ce22-3550-87b8-e5f76c1b039a"
            ]
          }
        }
      ]
    }
  }
}
EOF
}

gen_post_data2() {
cat <<EOF
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "${3}": {
              "from": "${1}",
              "to": "${2}",
              "include_lower": true,
              "include_upper": false
            }
          }
        },
        {
          "range": {
            "indexed": {
              "to": "${4}",
              "include_upper": false
            }
          }
        },
        {
          "terms": {
            "state.name": [
              "editing",
              "processing"
            ]
          }
        }
      ]
    }
  }
}
EOF
}



clean_articles() {
  local start=$1
  local end=$2
  local datefield=$3
  echo "curl -X POST ${url}/_delete_by_query" -H 'Content-Type: application/json' -d "$(gen_post_data ${start} ${end} ${datefield})"
  if ! curl -X POST "${url}/_delete_by_query" -H 'Content-Type: application/json' -d "$(gen_post_data ${start} ${end} ${datefield})"; then
    echo "Error while cleaning [${url}] for interval [${start} => ${end}] on [${datefield}]!"
    terminate 1
  fi
}

clean_stale_articles() {
  local start=$1
  local end=$2
  local datefield=$3
  local limit=$4
  echo "curl -X POST ${url}/_delete_by_query" -H 'Content-Type: application/json' -d "$(gen_post_data2 ${start} ${end} ${datefield} ${limit})"
  if ! curl -X POST "${url}/_delete_by_query" -H 'Content-Type: application/json' -d "$(gen_post_data2 ${start} ${end} ${datefield} ${limit})"; then
    echo "Error while cleaning stale [${url}] for interval [${start} => ${end}] on [${datefield}]!"
    terminate 1
  fi
}

echo "Cleaning [${url}] for interval [${start} => ${end}] on [${datefield}]!"
clean_articles "${start}" "${end}" "${datefield}"

echo "Cleaning stale [${url}] for interval [${start} => ${end}] on [${datefield}]!"
clean_stale_articles "${start}" "${end}" "${datefield}" "${stale_limit}"
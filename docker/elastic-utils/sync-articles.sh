#!/usr/bin/env bash

#input=http://localhost:59200/article
#output=http://localhost:9200/article-index
input="${1}"
output="${2}"
timespan="${3}"
datefield="${4}"

last_15min=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "15 minutes ago")
last_half_hour=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "30 minutes ago")
last_hour=$(date +%Y-%m-%dT%H:%M:00.000Z -u -d "1 hour ago")
tomorrow=$(date +%Y-%m-%dT00:00:00.000Z -d "tomorrow")
today=$(date +%Y-%m-%dT00:00:00.000Z -d "today")
yesterday=$(date +%Y-%m-%dT00:00:00.000Z -d "yesterday")
this_week=$(date +%Y-%m-%dT00:00:00.000Z -d "1 week ago")
this_month=$(date +%Y-%m-01T00:00:00.000Z -d "today")
prev_month=$(date +%Y/%m -d "1 month ago")

if [ -z "${input}" ]; then
  echo "Error: source elasticsearch argument is missing!"
  exit 1
fi

if [ -z "${output}" ]; then
  echo "Error: target elasticsearch argument is missing!"
  exit 1
fi

if [ -z "${timespan}" ]; then
  timespan="--last-15min"
fi

if [ -z "${datefield}" ]; then
  datefield="indexed"
fi

gen_post_data() {
cat <<EOF
{
  "query": {
    "range": {
      "${3}": {
        "from": "${1}",
        "to": "${2}",
        "include_lower": true,
        "include_upper": false
      }
    }
  },
  "sort": [
    {
      "created": {
        "order": "desc",
        "ignore_unmapped": true,
        "mode": "min"
      }
    }
  ]
}
EOF
}



sync_articles() {
  local start=$1
  local end=$2
  local datefield=$3
  echo "Syncing up [${start} => ${end}] on [${datefield}]!"

  if ! elasticdump \
    --input=${input} --output=${output} --data --limit 1000 --transform='doc._type="_doc"' \
    --searchBody="$(gen_post_data ${start} ${end} ${datefield})"; then
    echo "Error while syncing [${start} => ${end}] on [${datefield}]!"
    exit 1
  fi
}

case "${timespan}" in
  "--last-15min") start="${last_15min}"; end="${tomorrow}";;
  "--last-half-hour") start="${last_half_hour}"; end="${tomorrow}";;
  "--last-hour") start="${last_hour}"; end="${tomorrow}";;
  "--today") start="${today}"; end="${tomorrow}";;
  "--yesterday") start="${yesterday}"; end="${today}";;
  "--this-week") start="${this_week}"; end="${tomorrow}";;
  "--this-month") start="${this_month}"; end="${today}";;
  "--prev-month") start="${prev_month}"; end="${today}";;
  *) start="${today}"; end="${tomorrow}";;
esac

sync_articles "${start}" "${end}" "${datefield}"
exit 0
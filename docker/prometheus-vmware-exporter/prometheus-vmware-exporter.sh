#!/usr/bin/env sh
if [ -z ${MY_POD_NAME+x} ]; then
MY_POD_NAME=""
fi
K8S_STATEFULSET_INDEX=${MY_POD_NAME##*-}
if [ -z ${APPEND_K8S_SS_INDEX+x} ]; then
  echo "Using env host ${ESX_HOST}"
else
  export ESX_HOST=${ESX_HOST}${K8S_STATEFULSET_INDEX}
  echo "Using env host with appended index ${ESX_HOST}"
fi

prometheus-vmware-exporter
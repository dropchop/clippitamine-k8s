#!/usr/bin/env bash
#create local client contexts
kubectl config set-context clippitamine --user=kubernetes-admin --namespace=clippitamine --cluster=clippitamine
kubectl config set-context support --user=kubernetes-admin --namespace=support --cluster=clippitamine
kubectl config set-context kube-system --user=kubernetes-admin --namespace=kube-system --cluster=clippitamine
kubectl config use-context kube-system

########### join new nodes
#join other nodes
#kubeadm join kubernetes.dropchop.cloud:6443 --token incijl.i1ckqworzdu95xu8 --discovery-token-ca-cert-hash sha256:4ea78409d9610c8af4aeb534bc47a2b5efc285639bfde34bb324caea38ef4a31

########### join existing nodes - resetting them
#kubeadm reset
#systemctl stop kubelet
#rm -Rf /etc/kubernetes/*
#kubeadm join kubernetes.dropchop.cloud:6443 --token incijl.i1ckqworzdu95xu8 --discovery-token-ca-cert-hash sha256:4ea78409d9610c8af4aeb534bc47a2b5efc285639bfde34bb324caea38ef4a31
#reboot

#label other nodes in cluster
kubectl label node master-0 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=master
kubectl label node master-1 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=master
kubectl label node master-2 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=master
kubectl label node support-0 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=support
kubectl label node support-1 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=support
kubectl label node support-2 node-role.kubernetes.io/worker=worker clippitamine.io/tier=system clippitamine.io/class=support
kubectl label node data-0 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=db
kubectl label node data-1 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=db
kubectl label node data-2 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=db
kubectl label node fs-0 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=filesystem
kubectl label node fs-1 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=filesystem
kubectl label node fs-2 node-role.kubernetes.io/worker=worker clippitamine.io/tier=data clippitamine.io/class=filesystem

#TODO: worker labeling
kubectl label node worker-0 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=process
kubectl label node worker-1 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=process
kubectl label node worker-2 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=fetching
kubectl label node worker-3 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=creation
kubectl label node worker-4 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=creation
kubectl label node worker-5 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=distribution
kubectl label node worker-6 node-role.kubernetes.io/worker=worker clippitamine.io/tier=backend clippitamine.io/class=process clippitamine.io/subclass=distribution

#Deploy CNI - container networking (calico)
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml

#init helm -> install cluster component tiller
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-role --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --override 'spec.template.spec.tolerations[0].effect=NoSchedule' \
--override 'spec.template.spec.tolerations[0].key=node-role.kubernetes.io/master' \
--tiller-namespace kube-system --node-selectors "clippitamine.io/class"="master" \
--service-account tiller
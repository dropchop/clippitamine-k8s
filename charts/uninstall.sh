helm delete --purge clippitamine-data-ui
helm delete --purge clippitamine-data

kubectl delete pvc data-clippitamine-data-mongodb-0
kubectl delete pvc data-clippitamine-data-mongodb-1
kubectl delete pvc data-clippitamine-data-mongodb-2
kubectl delete pv mongo-0
kubectl delete pv mongo-1
kubectl delete pv mongo-2
kubectl delete pvc data-clippitamine-data-elasticsearch-0
kubectl delete pvc data-clippitamine-data-elasticsearch-1
kubectl delete pvc data-clippitamine-data-elasticsearch-2
kubectl delete pv elasticsearch-0
kubectl delete pv elasticsearch-1
kubectl delete pv elasticsearch-2
kubectl delete pvc data-clippitamine-data-rabbitmq-0
kubectl delete pvc data-clippitamine-data-rabbitmq-1
kubectl delete pvc data-clippitamine-data-rabbitmq-2
kubectl delete pv rabbitmq-0
kubectl delete pv rabbitmq-1
kubectl delete pv rabbitmq-2
kubectl delete pvc data-clippitamine-data-stolon-keeper-0
kubectl delete pvc data-clippitamine-data-stolon-keeper-1
kubectl delete pvc data-clippitamine-data-stolon-keeper-2
kubectl delete pv postgresql-0
kubectl delete pv postgresql-1
kubectl delete pv postgresql-2

ssh -T data-0 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && exit" && \
ssh -T data-1 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && exit" && \
ssh -T data-2 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && exit"

kubectl delete cm stolon-cluster-clippitamine-data-stolon
kubectl delete cm clippitamine-data-databases-scripts

helm delete --purge clippitamine-monitor
kubectl delete crd prometheuses.monitoring.coreos.com \
prometheusrules.monitoring.coreos.com \
servicemonitors.monitoring.coreos.com \
alertmanagers.monitoring.coreos.com

kubectl delete pvc data-clippitamine-monitor-elasticsearchlogging-0
kubectl delete pvc data-clippitamine-monitor-elasticsearchlogging-1
kubectl delete pvc data-clippitamine-monitor-elasticsearchlogging-2
kubectl delete pv elasticsearch-logging-0
kubectl delete pv elasticsearch-logging-1
kubectl delete pv elasticsearch-logging-2

ssh -T support-0 "rm -Rf /data/elasticsearch/* && exit" && \
ssh -T support-1 "rm -Rf /data/elasticsearch/* && exit" && \
ssh -T support-2 "rm -Rf /data/elasticsearch/* && exit"


helm delete --purge clippitamine-fs
#Delete gluster and wipe out volumes
kubectl delete daemonset clippitamine-fs-gluster-glusterfs
kubectl delete secrets heketi-db-backup

ssh -T fs-0 "rm -Rf /var/lib/glusterd && dd if=/dev/zero of=/dev/sdb bs=512 count=1 && dd if=/dev/zero of=/dev/sdb bs=512 count=2048 && dd if=/dev/zero of=/dev/sdc bs=512 count=1 && dd if=/dev/zero of=/dev/sdc bs=512 count=2048 && reboot"
ssh -T fs-1 "rm -Rf /var/lib/glusterd && dd if=/dev/zero of=/dev/sdb bs=512 count=1 && dd if=/dev/zero of=/dev/sdb bs=512 count=2048 && dd if=/dev/zero of=/dev/sdc bs=512 count=1 && dd if=/dev/zero of=/dev/sdc bs=512 count=2048 && reboot"
ssh -T fs-2 "rm -Rf /var/lib/glusterd && dd if=/dev/zero of=/dev/sdb bs=512 count=1 && dd if=/dev/zero of=/dev/sdb bs=512 count=2048 && dd if=/dev/zero of=/dev/sdc bs=512 count=1 && dd if=/dev/zero of=/dev/sdc bs=512 count=2048 && reboot"

helm delete --purge clippitamine-system
kubectl delete crd certificates.certmanager.k8s.io clusterissuers.certmanager.k8s.io issuers.certmanager.k8s.io





#ssh -T data-0 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && reboot"
#ssh -T data-1 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && reboot"
#ssh -T data-2 "rm -Rf /data/mongodb/* /data/elasticsearch/* /data/postgresql/* /data/rabbitmq/* && reboot"


{{- define "dashboard_ingress_auth_fix_py" -}}
import os
import base64
from kubernetes import client, config, watch


def main(ns: str, ingress_token_prefix: str) -> None:
    # it works only if this script is run by K8s as a POD
    config.load_incluster_config()
    #config.load_kube_config(os.path.join(os.environ["HOME"], '.kube/config'))

    v1_api = client.CoreV1Api()
    ext_api = client.ExtensionsV1beta1Api()
    ingresses = ext_api.list_namespaced_ingress(ns)
    for ingress in ingresses.items:
        ingress_name = ingress.metadata.name
        annos = ingress.metadata.annotations
        if 'kubernetes.io/ingress.class' not in annos:
            continue
        if not annos.get('kubernetes.io/ingress.class') == 'nginx':
            continue
        if 'nginx.ingress.kubernetes.io/configuration-snippet' not in annos:
            continue
        conf_snippet = annos.get('nginx.ingress.kubernetes.io/configuration-snippet')
        if conf_snippet.find(ingress_token_prefix) <= -1:
            continue
        print("Found token prefix %s in %s ingress. Will try to replace..." % (ingress_token_prefix, ingress_name))
        secrets = v1_api.list_namespaced_secret(ns)
        for secret in secrets.items:
            sec_name = secret.metadata.name
            sec_data = secret.data
            if not sec_name.startswith(ingress_token_prefix):
                continue
            if 'token' not in sec_data:
                continue
            print("Found token %s for prefix %s in secret for %s ingress. Will try to replace..." %
                  (sec_name, ingress_token_prefix, ingress_name))
            token = base64.b64decode(sec_data.get('token')).decode('utf-8', 'ignore')
            conf_snippet = conf_snippet.replace(ingress_token_prefix, token)
            annos['nginx.ingress.kubernetes.io/configuration-snippet'] = conf_snippet
            ext_api.replace_namespaced_ingress(ingress_name, ns, ingress)
            print("Replaced token %s for prefix %s in secret for %s ingress." %
                  (sec_name, ingress_token_prefix, ingress_name))


if __name__ == '__main__':
    main(os.environ["POD_NAMESPACE"], os.environ["DASHBOARD_TOKEN_NAME"])

{{- end -}}
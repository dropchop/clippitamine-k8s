{{- define "nginx_ingress_auth_fix_py" -}}
import os
import base64
from passlib.apache import HtpasswdFile
from kubernetes import client, config, watch


def main(ns: str) -> None:
    # it works only if this script is run by K8s as a POD
    config.load_incluster_config()
    # config.load_kube_config(os.path.join(os.environ["HOME"], '.kube/config'))

    v1_api = client.CoreV1Api()
    ext_api = client.ExtensionsV1beta1Api()
    ingresses = ext_api.list_namespaced_ingress(ns)
    for ingress in ingresses.items:
        ingress_name = ingress.metadata.name
        annos = ingress.metadata.annotations
        if 'kubernetes.io/ingress.class' not in annos:
            continue
        if not annos.get('kubernetes.io/ingress.class') == 'nginx':
            continue
        if 'nginx.ingress.kubernetes.io/auth-secret' not in annos:
            continue
        target_sec_name = annos.get('nginx.ingress.kubernetes.io/auth-secret')
        print("Found sec name %s in %s ingress. Will try to replace..." % (target_sec_name, ingress_name))
        secrets = v1_api.list_namespaced_secret(ns)
        for secret in secrets.items:
            sec_name = secret.metadata.name
            sec_data = secret.data
            if sec_name != target_sec_name:
                continue
            # if 'auth' in sec_data:
            #     print("Secret %s already has auth filed in data. Will not replace." % sec_name)
            #     continue
            if 'password' not in sec_data:
                print("Secret %s is missing password field in data. Will not replace." % sec_name)
                continue
            if 'username' not in sec_data:
                print("Secret %s is missing username field in data. Will not replace." % sec_name)
                continue
            username = base64.b64decode(sec_data['username'])
            password = base64.b64decode(sec_data['password'])
            ht = HtpasswdFile()
            ht.set_password(username, password)
            auth_data = ht.to_string()
            print("Secret %s auth data %s generated." % (sec_name, auth_data))
            auth_data = base64.b64encode(auth_data)
            print("Secret %s auth data %s encoded." % (sec_name, auth_data))
            sec_data['auth'] = auth_data.decode("utf-8", "ignore")
            v1_api.replace_namespaced_secret(sec_name, ns, secret)
            print("Secret %s replaced." % sec_name)


if __name__ == '__main__':
    main(os.environ["POD_NAMESPACE"])
{{- end -}}
#!/usr/bin/env bash

helm repo add https://dropchop.gitlab.io/clippitamine-k8s

kubectl config use-context kube-system
helm dependency update
helm install -f prod-values.yaml --name clippitamine-system infrastructure

kubectl config use-context clippitamine
helm install -f prod-values.yaml --name clippitamine-monitor monitoring
helm install -f prod-values.yaml --name clippitamine-fs gluster
helm install -f prod-values.yaml --name clippitamine-data databases
helm install -f prod-values.yaml --name clippitamine-data-ui data-ui



ssh -TL 55432:10.44.1.5:5432 root@kubernetes.dropchop.cloud
kubectl port-forward svc/clippitamine-data-stolon-proxy 55432:5432

cat << 'EOF' >> ~/.pgpass
localhost:55432:*:admin:Dpxpd2WzkpjVQ6RJ
localhost:55432:*:clippitamine:clippitamine
localhost:5432:*:clippitamine:clippitamine
EOF
chmod 600 ~/.pgpass

#CREATE DATABASE clippitamine OWNER clippitamine TEMPLATE 'template0' ENCODING 'UTF-8' LC_COLLATE 'sl_SI.UTF-8' LC_CTYPE 'sl_SI.UTF-8';

psql --host localhost --port 55432 postgres -U postgres << EOF
CREATE USER clippitamine;
ALTER USER clippitamine WITH PASSWORD 'clippitamine';
CREATE USER clippitamine_ro;
ALTER USER clippitamine_ro WITH PASSWORD 'clippitamine_ro';
CREATE USER dropchopdev;
ALTER USER dropchopdev WITH PASSWORD 'oseventintar';
CREATE DATABASE clippitamine OWNER clippitamine TEMPLATE 'template0' ENCODING 'UTF-8' LC_COLLATE 'sl-SI-x-icu' LC_CTYPE 'sl-SI-x-icu';
GRANT ALL PRIVILEGES ON DATABASE clippitamine TO clippitamine;
GRANT CONNECT ON DATABASE clippitamine TO clippitamine_ro;
GRANT CONNECT ON DATABASE clippitamine TO dropchopdev;
\connect clippitamine postgres
EOF

psql --host localhost --port 55432 clippitamine -U postgres << EOF
CREATE EXTENSION "uuid-ossp";
ALTER USER clippitamine SUPERUSER
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO dropchopdev;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO kliping2_ro;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON SEQUENCES TO dropchopdev;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON SEQUENCES TO kliping2_ro;
EOF

psql --host localhost --port 55432 clippitamine -U clippitamine << EOF
CREATE SCHEMA mock;
GRANT ALL PRIVILEGES ON SCHEMA mock TO dropchopdev;
GRANT USAGE ON SCHEMA public TO dropchopdev;
GRANT USAGE ON SCHEMA public TO clippitamine_ro;
EOF


psql --host localhost --port 55432 postgres -U postgres << EOF
CREATE USER clippitamine;
ALTER USER clippitamine WITH PASSWORD 'clippitamine';
CREATE USER clippitamine_ro;
ALTER USER clippitamine_ro WITH PASSWORD 'clippitamine_ro';
CREATE USER dropchopdev;
ALTER USER dropchopdev WITH PASSWORD 'oseventintar';
CREATE DATABASE clippitamine OWNER clippitamine TEMPLATE 'template0' ENCODING 'UTF-8' LC_COLLATE 'sl_SI.UTF-8' LC_CTYPE 'sl_SI.UTF-8';
GRANT ALL PRIVILEGES ON DATABASE clippitamine TO clippitamine;
GRANT CONNECT ON DATABASE clippitamine TO clippitamine_ro;
GRANT CONNECT ON DATABASE clippitamine TO dropchopdev;
\connect clippitamine postgres
CREATE EXTENSION "uuid-ossp";
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO dropchopdev;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO clippitamine_ro;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON SEQUENCES TO dropchopdev;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON SEQUENCES TO clippitamine_ro;
CREATE SCHEMA mock;
ALTER SCHEMA mock OWNER TO clippitamine;
GRANT ALL PRIVILEGES ON SCHEMA mock TO dropchopdev;
GRANT USAGE ON SCHEMA public TO dropchopdev;
GRANT USAGE ON SCHEMA public TO clippitamine_ro;
EOF



/usr/bin/pg_restore --disable-triggers --host localhost --port 55432 -U clippitamine -O -S postgres -s -d clippitamine kliping_micro_struct.pg_dump
/usr/bin/pg_restore --disable-triggers --host localhost --port 55432 -U clippitamine -O -S postgres -a -d clippitamine kliping_micro_data.pg_dump



